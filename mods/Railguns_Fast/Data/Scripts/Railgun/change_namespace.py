import os

old = raw_input('Old Namespace: ')
new = raw_input('New namespace: ')
prefixes = ['namespace', 'using']

count = 0
modified = []

for root, subdirs, files in os.walk(os.getcwd()):
    for filename in files:
        path = os.path.join(root, filename)
        if not filename.endswith('.cs'):
            continue
        with open(path, 'r') as f:
            text = f.read()
        found = False
        for prefix in prefixes:
            old_pre = prefix + ' ' + old
            new_pre = prefix + ' ' + new
            if old_pre in text:
                text = text.replace(old_pre, new_pre)
                found = True
        if found:
            with open(path, 'w') as f:
                f.write(text)
            modified.append(path)
            count = count + 1

print('Modified ' + str(count) + ' file(s):')
for p in modified:
    print('  ' + p)
