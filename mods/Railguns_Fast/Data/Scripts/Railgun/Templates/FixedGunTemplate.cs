﻿using Sandbox.Common.ObjectBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game.Components;

namespace Whiplash.Railgun
{
    /*
    ** Replace the subtype and class name below.
    */
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_SmallMissileLauncher), false, "REPLACE_WITH_FIXED_GUN_SUBTYPE_NAME")]
    public class REPLACE_WITH_FIXED_GUN_CLASS_NAME : WeaponBlockFixedBase
    {
    }
}
