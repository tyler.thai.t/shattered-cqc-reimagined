﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRageMath;

namespace Whiplash.Railgun
{
    class WeaponConstants
    {
        public static Dictionary<Type, WeaponConfig> RailgunConfigDict = new Dictionary<Type, WeaponConfig>()
        {
            // Create config for fixed railgun
            {
                typeof(FixedRailgun),
                new WeaponConfig()
                {
                    ConfigSectionName = "Railgun",
                    ConfigFileName = "RailgunConfig.sbc",

                    FireSoundName = "WepShipLargeRailgunShotLZM",
                    FireSoundVolumeMultiplier = 1f,

                    ShowReloadMessage = true,
                    ReloadMessage = "Railgun reloading",

                    DrawMuzzleFlash = false,
                    MuzzleFlashSpriteName = "Muzzle_Flash_Large",
                    MuzzleFlashDuration = 0.1f,
                    MuzzleFlashScale = 1f,

                    TracerColor = new Vector3(0, 0.5f, 1f),
                    TracerScale = 5,
                    ShouldDrawProjectileTrails = true,
                    ProjectileTrailFadeRatio = 0.03f,

                    BulletSpawnForwardOffsetMeters = -3,

                    ArtificialGravityMultiplier = 2f,
                    NaturalGravityMultiplier = 1f,

                    ExplodeOnContact = true,
                    ContactExplosionRadius = 0f,
                    ContactExplosionDamage = 0f,

                    PenetrateOnContact = true,
                    PenetrationDamage = 33000,
                    PenetrationRange = 100f,

                    ExplodePostPenetration = false,
                    PenetrationExplosionRadius = 0f,
                    PenetrationExplosionDamage = 0f,

                    IdlePowerDrawBase = 2f,
                    ReloadPowerDraw = 200f,
                    MuzzleVelocity = 5000f,
                    MaxRange = 5000,
                    DeviationAngleDeg = 0.01f,
                    RecoilImpulse = 1000000,
                    ShieldDamageMultiplier = 5,
                    RateOfFireRPM = 5,
                    HitImpulse = 1000000,
                }
            },

            // Create config for turret railgun
            {
                typeof(TurretRailgun),
                new TurretWeaponConfig()
                {
                    ConfigSectionName = "Railgun Turret",
                    ConfigFileName = "RailgunTurretConfig.sbc",

                    FireSoundName = "WepShipLargeRailgunShotLZM",
                    FireSoundVolumeMultiplier = 1f,

                    ShowReloadMessage = true,
                    ReloadMessage = "Railgun reloading",

                    DrawMuzzleFlash = false,
                    MuzzleFlashSpriteName = "Muzzle_Flash_Large",
                    MuzzleFlashDuration = 0.1f,
                    MuzzleFlashScale = 1f,

                    TracerColor = new Vector3(0, 0.5f, 1f),
                    TracerScale = 5,
                    ShouldDrawProjectileTrails = true,
                    ProjectileTrailFadeRatio = 0.03f,

                    BulletSpawnForwardOffsetMeters = -3,

                    ArtificialGravityMultiplier = 2f,
                    NaturalGravityMultiplier = 1f,

                    ExplodeOnContact = true,
                    ContactExplosionRadius = 0f,
                    ContactExplosionDamage = 0f,

                    PenetrateOnContact = true,
                    PenetrationDamage = 33000,
                    PenetrationRange = 100f,

                    ExplodePostPenetration = false,
                    PenetrationExplosionRadius = 0f,
                    PenetrationExplosionDamage = 0f,

                    IdlePowerDrawBase = 2f,
                    IdlePowerDrawMax = 20f,
                    ReloadPowerDraw = 200f,

                    MuzzleVelocity = 5000f,
                    MaxRange = 5000,
                    DeviationAngleDeg = 0.01f,
                    RecoilImpulse = 1000000,
                    ShieldDamageMultiplier = 5,
                    RateOfFireRPM = 5,
                    HitImpulse = 1000000,
                }
            }
        };

        // Tag for debugging
        public const string DEBUG_MSG_TAG = "Railgun Mod";

        // Network traffic IDs
        public const ushort NETID_FIRE_SYNC = 1507;
        public const ushort NETID_RECHARGE_SYNC = 3896; 
    }
}