﻿using VRageMath;

namespace Whiplash.Railgun
{
    public class WeaponConfig
    {
        // Not ini configurable
        public string ConfigSectionName;
        public string ConfigFileName;
        public string FireSoundName;
        public bool DrawMuzzleFlash;
        public string MuzzleFlashSpriteName;
        public float MuzzleFlashDuration;
        public float MuzzleFlashScale;
        public float BulletSpawnForwardOffsetMeters;
        public bool ShowReloadMessage;
        public string ReloadMessage;
        public float FireSoundVolumeMultiplier;
        public float HitImpulse;

        // Ini configurable
        public Vector3 TracerColor;
        public float TracerScale;
        public float ArtificialGravityMultiplier;
        public float NaturalGravityMultiplier;
        public bool ShouldDrawProjectileTrails;
        public float ProjectileTrailFadeRatio;
        public bool ExplodeOnContact;
        public float ContactExplosionRadius;
        public float ContactExplosionDamage;
        public bool PenetrateOnContact;
        public float PenetrationDamage;
        public float PenetrationRange;
        public bool ExplodePostPenetration;
        public float PenetrationExplosionRadius;
        public float PenetrationExplosionDamage;
        public float IdlePowerDrawBase;
        public float ReloadPowerDraw;
        public float MuzzleVelocity;
        public float MaxRange;
        public float DeviationAngleDeg;
        public float RecoilImpulse;
        public float ShieldDamageMultiplier;
        public float RateOfFireRPM;
    }

    public class TurretWeaponConfig : WeaponConfig
    {
        public float IdlePowerDrawMax;
    }
}

// fade rate