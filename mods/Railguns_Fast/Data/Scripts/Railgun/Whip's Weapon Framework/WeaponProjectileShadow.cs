﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using VRage.Game;
using VRage.Game.Components;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using Sandbox.Game.Weapons;
using VRage.Game.ModAPI;
using VRageMath;
using Sandbox.Game;
using VRage.Game.Entity;
using Sandbox.Game.Entities;
using VRage.Game.ModAPI.Interfaces;
using Sandbox.Definitions;
using Whiplash.Railgun;
using VRage.Collections;
using VRage.Voxels;
using Whiplash.WeaponTracers;

namespace Whiplash.WeaponProjectiles
{
    public class WeaponProjectileShadow
    {
        public bool Remove { get; private set; } = false;
        
        readonly bool _drawTrail;

        readonly Vector3 _tracerColor;
        readonly float _tracerScale;
        readonly Vector3D _origin;
        Vector3D _lastCheckedPosition;
        Vector3D _position;
        Vector3D _lastPosition;
        Vector3D _velocity;
        Vector3D _lastVelocity;
        Vector3D _direction;
        Vector3D _trailFrom;
        Vector3D _trailTo;
        Vector4 _trailColor;
        readonly float _trailDecayRatio;
        bool _hasDrawnTracer = false;
        bool _targetHit = false;
        bool _hitMaxTrajectory = false;
        bool _positionChecked = false;
        double _naturalGravityMult;
        double _artificialGravityMult;
        double _maxTrajectory;
        const double Tick = 1.0 / 60.0;
        readonly List<MyLineSegmentOverlapResult<MyVoxelBase>> _voxelOverlap = new List<MyLineSegmentOverlapResult<MyVoxelBase>>();
        WeaponTracer _currentTracer;
        int _checkIntersectionIndex = 5;
        long _shooterID;

        public WeaponProjectileShadow(WeaponFireSyncData fireSyncData)
        {
            _tracerColor = fireSyncData.TracerColor;
            _trailColor = new Vector4(_tracerColor, 1f);
            _tracerScale = fireSyncData.ProjectileTrailScale;
            _velocity = fireSyncData.ShooterVelocity + fireSyncData.Direction * fireSyncData.MuzzleVelocity;
            _lastVelocity = _velocity;
            _direction = Vector3.Normalize(_velocity);
            _drawTrail = fireSyncData.DrawTrails;
            _trailDecayRatio = fireSyncData.TrailDecayRatio;
            _naturalGravityMult = fireSyncData.NatGravityMult;
            _artificialGravityMult = fireSyncData.ArtGravityMult;
            _maxTrajectory = fireSyncData.MaxRange;
            _shooterID = fireSyncData.ShooterID;

            _origin = fireSyncData.Origin;

            _lastCheckedPosition = _origin;
            _trailFrom = _origin;
            _trailTo = _origin;
            _lastPosition = _origin;
            _position = _origin + WeaponProjectile.RaycastDelayTicks * _velocity * Tick; // Step forward in time
        }

        public void Update()
        {
            SimulateShadow();
            if (_targetHit)
            {
                DrawLastTracer();
                Remove = true;
            }
            WeaponCore.AddTracer(_currentTracer);

        }

        public void DrawLastTracer()
        {
            _currentTracer.To = _position;
        }

        public void SimulateShadow()
        {
            // Update velocity due to gravity
            Vector3D totalGravity = MyParticlesManager.CalculateGravityInPoint(_position);
            Vector3D naturalGravity = WeaponCore.GetNaturalGravityAtPoint(_position);
            Vector3D artificialGravity = totalGravity - naturalGravity;
            _velocity += (naturalGravity * _naturalGravityMult + artificialGravity * _artificialGravityMult) * Tick;

            // Update direction if velocity has changed
            if (!_velocity.Equals(_lastVelocity, 1e-3))
                _direction = Vector3D.Normalize(_velocity);

            _lastVelocity = _velocity;

            // Update position
            _lastPosition = _position;
            _position += _velocity * Tick;
            var _toOrigin = _position - _origin;

            _trailFrom = _trailTo;
            var stepBackDist = _velocity * Tick * WeaponProjectile.RaycastDelayTicks;
            _trailTo = _position - stepBackDist;

            // Get important bullet parameters
            float lengthMultiplier = 40f * _tracerScale;
            lengthMultiplier *= 0.6f;

            bool shouldDraw = false;
            if (lengthMultiplier > 0f && !_targetHit && Vector3D.DistanceSquared(_trailTo, _origin) > lengthMultiplier * lengthMultiplier) // && !Killed)
                shouldDraw = true;

            _currentTracer = new WeaponTracer(
                _direction,
                _trailFrom,
                _trailTo,
                _tracerColor,
                _tracerScale,
                _trailDecayRatio,
                shouldDraw,
                _drawTrail);

            if (_hitMaxTrajectory)
            {
                _targetHit = true;
                return;
            }

            if (_toOrigin.LengthSquared() > _maxTrajectory * _maxTrajectory)
            {
                MyLog.Default.WriteLine(">> Shadow: Max range hit");
                _hitMaxTrajectory = true;
                _positionChecked = false;
            }

            _checkIntersectionIndex = ++_checkIntersectionIndex % WeaponProjectile.RaycastDelayTicks;
            if (_checkIntersectionIndex != 0 && _positionChecked)
            {
                return;
            }

            var to = _position;
            var from = _lastCheckedPosition;
            _positionChecked = true;
            _lastCheckedPosition = _position;

            IHitInfo hitInfo;
            bool hit = false;

            if (Vector3D.DistanceSquared(to, from) > 50 * 50)
            {
                // Use faster raycast if ray is long enough
                hit = MyAPIGateway.Physics.CastLongRay(from, to, out hitInfo, true);
            }
            else
            {
                hit = MyAPIGateway.Physics.CastRay(from, to, out hitInfo, 0);
            }

            // DS - Shield hit intersection and damage
            if (WeaponCore.Instance.ShieldApiLoaded)
            {
                var checkLine = new LineD(from, to);
                var shieldInfo = WeaponCore.Instance.ShieldApi.ClosestShieldInLine(checkLine, true);
                if (hit && shieldInfo.Item1.HasValue && hitInfo.Fraction * checkLine.Length > shieldInfo.Item1.Value || shieldInfo.Item1.HasValue)
                {
                    _position = from + (checkLine.Direction * shieldInfo.Item1.Value);
                    _targetHit = true;
                    return;
                }
            }

            if (hit)
            {
                MyLog.Default.WriteLine(">> Raycast hit");
                if (WeaponProjectile.ShouldRegisterHit(hitInfo, _shooterID))
                {
                    _position = hitInfo.Position;
                    _targetHit = true;
                    return;
                }
            }
            // implied else
            var line = new LineD(from, to);
            MyGamePruningStructure.GetVoxelMapsOverlappingRay(ref line, _voxelOverlap);
            foreach (var result in _voxelOverlap)
            {
                MyPlanet planet = result.Element as MyPlanet;
                MyVoxelMap voxelMap = result.Element as MyVoxelMap;

                IMyEntity hitEntity = null;
                Vector3D? hitPos = null;
                if (planet != null)
                {
                    planet.GetIntersectionWithLine(ref line, out hitPos);
                    hitEntity = planet;
                }
                if (voxelMap != null)
                {
                    voxelMap.GetIntersectionWithLine(ref line, out hitPos);
                    hitEntity = voxelMap;
                }

                if (hitPos.HasValue)
                {
                    _position = hitPos.Value;
                    return;
                }
            }
        }

        
    }
}
