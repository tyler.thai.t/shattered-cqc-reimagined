﻿using System;
using System.Text;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using VRage.Game;
using VRage.Game.Components;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using Sandbox.Game.Weapons;
using VRage.Game.ModAPI;
using VRageMath;
using VRage.Game.Entity;
using Sandbox.Game.Entities;
using VRage.Game.ModAPI.Interfaces;
using Sandbox.Definitions;
using Sandbox.ModAPI.Interfaces.Terminal;
using SpaceEngineers.Game.ModAPI;
using Rexxar;

namespace Whiplash.Railgun
{
    public abstract class WeaponBlockTurretBase : WeaponBlockBase
    {
        static bool _terminalControlsInit = false;

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            base.Init(objectBuilder);

            IsTurret = true;
            try
            {
                CreateCustomTerminalControls<IMyLargeGatlingTurret>(ref _terminalControlsInit, cube.BlockDefinition.SubtypeId);
            }
            catch (Exception e)
            {

                MyAPIGateway.Utilities.ShowNotification("Exception in railgun turret init", 10000, MyFontEnum.Red);
                MyLog.Default.WriteLine(e);
            }
        }
    }

    public abstract class WeaponBlockFixedBase : WeaponBlockBase
    {
        static bool _terminalControlsInit = false;

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            base.Init(objectBuilder);

            IsTurret = false;
            try
            {
                CreateCustomTerminalControls<IMySmallMissileLauncher>(ref _terminalControlsInit, cube.BlockDefinition.SubtypeId);
            }
            catch (Exception e)
            {

                MyAPIGateway.Utilities.ShowNotification("Exception in fixed railgun init", 10000, MyFontEnum.Red);
                MyLog.Default.WriteLine(e);
            }
        }
    }

    public struct WeaponFireData
    {
        public Vector3D Origin;
        public Vector3D Direction;
        public Vector3D ShooterVelocity;
        public long ShooterID;
    }

    public abstract class WeaponBlockBase : MyGameLogicComponent
    {
        #region Member Fields
        public bool IsTurret { get; protected set; }
        public bool SettingsDirty;
        public bool ShouldShoot
        {
            get
            {
                return _lastIsWorking 
                        && !_isReloading
                        && (userControllableGun.IsShooting || TerminalPropertyExtensions.GetValue<bool>(userControllableGun, "Shoot"));
            }
        }

        protected IMyCubeBlock cube;
        IMyUserControllableGun userControllableGun;
        IMyGunObject<MyGunBase> gun;
        IMyFunctionalBlock block;
        IMyLargeTurretBase turret;
        float _turretMaxRange;
        float _shootInterval;
        float _reloadTicks;
        float _currentReloadTicks = 0;
        int _ticksSinceLastReload = 0;
        bool _isReloading = false;
        bool _firstUpdate = true;
        bool _init = false;
        bool _lastIsWorking = false;
        float _idlePowerDrawBase;
        float _idlePowerDrawMax = 1;
        float _reloadPowerDraw;
        float _trailScale;
        private static readonly MyDefinitionId resourceId = MyResourceDistributorComponent.ElectricityId;
        Vector3 _trailColor;
        MyResourceSinkComponent sink;
        MySoundPair _shootSound;
        MyEntity3DSoundEmitter soundEmitter;
        MyDefinitionId _definitionId;
        MyParticleEffect _muzzleFlash;
        protected WeaponConfig _config;
        double _secondsSinceMuzzleFlashSpawned = 0;
        bool _muzzleFlashSpawned = false;

        #endregion

        #region Terminal Action/Property Methods
        public void CreateCustomTerminalControls<T>(ref bool controlsInit, string subtypeName) where T : class, IMyTerminalBlock
        {
            if (controlsInit)
                return;

            controlsInit = true;

            IMyTerminalControlOnOffSwitch rechargeControl = MyAPIGateway.TerminalControls.CreateControl<IMyTerminalControlOnOffSwitch, T>("RechargeRailgun");
            rechargeControl.Title = MyStringId.GetOrCompute("Recharge Railgun");
            rechargeControl.Enabled = x => x.BlockDefinition.SubtypeId.Equals(subtypeName);
            rechargeControl.Visible = x => x.BlockDefinition.SubtypeId.Equals(subtypeName);
            rechargeControl.SupportsMultipleBlocks = true;
            rechargeControl.OnText = MyStringId.GetOrCompute("On");
            rechargeControl.OffText = MyStringId.GetOrCompute("Off");
            rechargeControl.Setter = (x, v) => SetRecharging(x, v);
            rechargeControl.Getter = x => GetRecharging(x);
            MyAPIGateway.TerminalControls.AddControl<T>(rechargeControl);

            //Recharge toggle action
            IMyTerminalAction rechargeOnOff = MyAPIGateway.TerminalControls.CreateAction<T>("Recharge_OnOff");
            rechargeOnOff.Action = (x) =>
            {
                var recharge = GetRecharging(x);
                SetRecharging(x, !recharge);
            };
            rechargeOnOff.ValidForGroups = true;
            rechargeOnOff.Writer = (x, s) => GetWriter(x, s);
            rechargeOnOff.Icon = @"Textures\GUI\Icons\Actions\Toggle.dds";
            rechargeOnOff.Enabled = x => x.BlockDefinition.SubtypeId.Equals(subtypeName);
            rechargeOnOff.Name = new StringBuilder("Recharge On/Off");
            MyAPIGateway.TerminalControls.AddAction<T>(rechargeOnOff);

            //Recharge on action
            IMyTerminalAction rechargeOn = MyAPIGateway.TerminalControls.CreateAction<T>("Recharge_On");
            rechargeOn.Action = (x) => SetRecharging(x, true);
            rechargeOn.ValidForGroups = true;
            rechargeOn.Writer = (x, s) => GetWriter(x, s);
            rechargeOn.Icon = @"Textures\GUI\Icons\Actions\SwitchOn.dds";
            rechargeOn.Enabled = x => x.BlockDefinition.SubtypeId.Equals(subtypeName);
            rechargeOn.Name = new StringBuilder("Recharge On");
            MyAPIGateway.TerminalControls.AddAction<T>(rechargeOn);

            //Recharge off action
            IMyTerminalAction rechargeOff = MyAPIGateway.TerminalControls.CreateAction<T>("Recharge_Off");
            rechargeOff.Action = (x) => SetRecharging(x, false);
            rechargeOff.ValidForGroups = true;
            rechargeOff.Writer = (x, s) => GetWriter(x, s);
            rechargeOff.Icon = @"Textures\GUI\Icons\Actions\SwitchOff.dds";
            rechargeOff.Enabled = x => x.BlockDefinition.SubtypeId.Equals(subtypeName);
            rechargeOff.Name = new StringBuilder("Recharge Off");
            MyAPIGateway.TerminalControls.AddAction<T>(rechargeOff);
        }

        public void GetWriter(IMyTerminalBlock x, StringBuilder s)
        {
            s.Clear();
            var y = x.GameLogic.GetAs<WeaponBlockBase>();
            var set = Settings.GetSettings(x);

            if (y != null)
            {
                if (set.Recharging)
                    s.Append("On");
                else
                    s.Append("Off");
            }
        }

        public void SetRecharging(IMyTerminalBlock b, bool v)
        {
            var s = Settings.GetSettings(b);
            s.Recharging = v;
            Settings.SetSettings(b, s);
            SetDirty(b);
        }

        public bool GetRecharging(IMyTerminalBlock b)
        {
            return Settings.GetSettings(b).Recharging;
        }

        public void SetDirty(IMyTerminalBlock b)
        {
            var g = b.GameLogic.GetAs<WeaponBlockBase>();
            if (g != null)
                g.SettingsDirty = true;
        }
        #endregion

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            base.Init(objectBuilder);
            try
            {
                NeedsUpdate = MyEntityUpdateEnum.EACH_FRAME | MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
                // this.m_missileAmmoDefinition = weaponProperties.GetCurrentAmmoDefinitionAs<MyMissileAmmoDefinition>();
                cube = (IMyCubeBlock)Entity;
                block = (IMyFunctionalBlock)Entity;
                userControllableGun = (IMyUserControllableGun)Entity;
                gun = Entity as IMyGunObject<MyGunBase>;

                bool hasConfig = WeaponConstants.RailgunConfigDict.TryGetValue(this.GetType(), out _config);
                if (!hasConfig)
                {
                    MyAPIGateway.Utilities.ShowNotification($"ERROR: Config not found for type: {this.GetType()}", font: "Red");
                    MyLog.Default.WriteLine($"{WeaponConstants.DEBUG_MSG_TAG}: ERROR - Config not found for type: {this.GetType()}");
                }

                _idlePowerDrawBase = _config.IdlePowerDrawBase; //MW

                var turretConfig = _config as TurretWeaponConfig;
                if (turretConfig != null)
                {
                    _idlePowerDrawMax = turretConfig.IdlePowerDrawMax;
                }
                else
                {
                    _idlePowerDrawMax = _config.IdlePowerDrawBase;
                }

                _reloadPowerDraw = _config.ReloadPowerDraw; //MW
                SetAmmoProperties();
                GetTurretMaxRange();
                SetPowerSink();
            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowNotification("Exception in init", 10000, MyFontEnum.Red);
                MyLog.Default.WriteLine(e);
            }
        }

        public void GetBulletOriginAndDirection(ref Vector3D origin, ref Vector3D direction)
        {
            MatrixD muzzleMatrix = gun.GunBase.GetMuzzleWorldMatrix();
            direction = muzzleMatrix.Forward;
            Vector3D offset = direction * _config.BulletSpawnForwardOffsetMeters;
            origin = muzzleMatrix.Translation + offset;
        }

        public override void UpdateOnceBeforeFrame()
        {
            base.UpdateOnceBeforeFrame();
            try
            {
                // We fetch these again to compensate for the config update.
                _idlePowerDrawBase = _config.IdlePowerDrawBase; //MW

                var turretConfig = _config as TurretWeaponConfig;
                if (turretConfig != null)
                {
                    _idlePowerDrawMax = turretConfig.IdlePowerDrawMax;
                }
                else
                {
                    _idlePowerDrawMax = _config.IdlePowerDrawBase;
                }

                _reloadPowerDraw = _config.ReloadPowerDraw; //MW

                _trailScale = _config.TracerScale;
                _shootSound = new MySoundPair(_config.FireSoundName);

                ComputeRateOfFireParameters();
                GetTurretPowerDrawConstants(_idlePowerDrawBase, _idlePowerDrawMax, _turretMaxRange); // Update power draw constants
                _init = true;
            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowNotification("Exception in update once", 10000, MyFontEnum.Red);
                MyLog.Default.WriteLine(e);
            }
        }

        public override void UpdateAfterSimulation()
        {
            base.UpdateAfterSimulation();
        }

        public override void UpdateBeforeSimulation()
        {
            if (_init && MyAPIGateway.Multiplayer.IsServer && gun.GunBase.HasEnoughAmmunition())
                gun.GunBase.CurrentAmmo = 1;

            base.UpdateBeforeSimulation();

            if (!_init)
                return;

            try
            {
                //MyAPIGateway.Utilities.ShowNotification($"phys:{cube?.CubeGrid?.Physics != null} | enab:{userControllableGun.Enabled} | func:{userControllableGun.IsFunctional} | work:{_lastIsWorking} | shoot:{userControllableGun.IsShooting} | tog: {TerminalPropertyExtensions.GetValue<bool>(userControllableGun, "Shoot")}", 16);

                if (cube?.CubeGrid?.Physics == null) //ignore ghost grids
                    return;

                if (ShouldShoot && !_isReloading && !_firstUpdate) //Shoot
                {
                    
                    _isReloading = true;
                    soundEmitter = new MyEntity3DSoundEmitter((MyEntity)Entity, true);
                    soundEmitter.CustomVolume = _config.FireSoundVolumeMultiplier;
                    soundEmitter.PlaySingleSound(_shootSound, true);

                    //MyAPIGateway.Utilities.ShowNotification($"curr ammo:{gun.GunBase.CurrentAmmo}");

                    Vector3D direction = Vector3D.Zero;
                    Vector3D origin = Vector3D.Zero;
                    GetBulletOriginAndDirection(ref origin, ref direction);

                    // Fire weapon
                    if (MyAPIGateway.Multiplayer.IsServer)
                    {
                        gun.GunBase.CurrentAmmo = 0;
                        gun.GunBase.ConsumeAmmo();

                        var velocity = block.CubeGrid.Physics.LinearVelocity;

                        var fireData = new WeaponFireData()
                        {
                            ShooterVelocity = velocity,
                            Origin = origin,
                            Direction = direction,
                            ShooterID = Entity.EntityId,
                        };

                        WeaponCore.ShootProjectile(fireData, _config);

                        _currentReloadTicks = 0;

                        //Apply recoil force
                        var centerOfMass = block.CubeGrid.Physics.CenterOfMassWorld;
                        var forceVector = -direction * _config.RecoilImpulse;

                        block.CubeGrid.Physics.AddForce(MyPhysicsForceType.APPLY_WORLD_IMPULSE_AND_WORLD_ANGULAR_IMPULSE, forceVector, block.GetPosition(), null);
                    }

                    if (!MyAPIGateway.Utilities.IsDedicated && _config.DrawMuzzleFlash)
                    {
                        MatrixD matrix = MatrixD.CreateFromDir(-direction); //Nagative because muzzle flashes are fucking backwards
                        matrix.Translation = origin;
                        bool foundParticle = MyParticlesManager.TryCreateParticleEffect(_config.MuzzleFlashSpriteName, ref matrix, ref origin, uint.MaxValue, out _muzzleFlash);
                        if (foundParticle)
                        {
                            _muzzleFlash.UserEmitterScale = _config.MuzzleFlashScale;
                            _muzzleFlash.Loop = false;
                            _muzzleFlash.Play();
                            _muzzleFlashSpawned = true;
                            _secondsSinceMuzzleFlashSpawned = 0;
                        }
                    }
                }

                if (!MyAPIGateway.Utilities.IsDedicated && _muzzleFlashSpawned)
                {
                    if (_secondsSinceMuzzleFlashSpawned >= _config.MuzzleFlashDuration)
                    {
                        if (_muzzleFlash != null)
                            _muzzleFlash.Stop();
                        _muzzleFlashSpawned = false;
                    }
                    else
                        _secondsSinceMuzzleFlashSpawned += (1.0 / 60.0);
                }

                _lastIsWorking = userControllableGun.IsFunctional && userControllableGun.Enabled;

                _firstUpdate = false;
                ShowReloadMessage();
                sink.Update();
            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowNotification("Exception in update", 960, MyFontEnum.Red);
                MyLog.Default.WriteLine(e);
            }

            
        }

        void SetAmmoProperties()
        {
            //-----------------------------------------------------------------
            //Thanks digi <3
            var slim = block.SlimBlock; //.CubeGrid.GetCubeBlock(block.Position);
            var definition = slim.BlockDefinition;
            var weapon = (MyWeaponBlockDefinition)definition;
            _definitionId = weapon.WeaponDefinitionId;
            var wepDef = MyDefinitionManager.Static.GetWeaponDefinition(_definitionId);

            for (int i = 0; i < wepDef.AmmoMagazinesId.Length; i++)
            {
                var mag = MyDefinitionManager.Static.GetAmmoMagazineDefinition(wepDef.AmmoMagazinesId[i]);
                var ammo = MyDefinitionManager.Static.GetAmmoDefinition(mag.AmmoDefinitionId);

                ammo.MaxTrajectory = _config.MaxRange;

                var projectileAmmo = ammo as MyProjectileAmmoDefinition;
                projectileAmmo.ProjectileTrailProbability = 0f; //disable default tracers
                projectileAmmo.ProjectileMassDamage = 0f;
                projectileAmmo.ProjectileHealthDamage = 0f;
                projectileAmmo.BackkickForce = 0f;
                projectileAmmo.ProjectileHitImpulse = 0f;

                _trailColor = projectileAmmo.ProjectileTrailColor; //TODO: Move this to config?

            }

            for (int i = 0; i < wepDef.WeaponAmmoDatas.Length; ++i)
            {
                var ammoData = wepDef.WeaponAmmoDatas[i];

                if (ammoData == null)
                    continue;

                ammoData.ShootIntervalInMiliseconds = 17;  //(int)(60f / _config.RateOfFireRPM * 1000f);
            }


            //-------------------------------------------
        }

        void ComputeRateOfFireParameters()
        {
            //Compute reload ticks
            _shootInterval = 60f / _config.RateOfFireRPM;  // gun.GunBase.ShootIntervalInMiliseconds; //wepDef.ReloadTime;
            _reloadTicks = (_shootInterval * 60f); // + 1;
        }

        void GetTurretMaxRange()
        {
            //init turret power draw function constants
            if (Entity is IMyLargeTurretBase)
            {
                turret = Entity as IMyLargeTurretBase;
                var def = cube.SlimBlock.BlockDefinition as MyLargeTurretBaseDefinition;
                _turretMaxRange = def.MaxRangeMeters;
                var ob = (MyObjectBuilder_TurretBase)cube.GetObjectBuilderCubeBlock();
                ob.Range = _turretMaxRange;
                GetTurretPowerDrawConstants(_idlePowerDrawBase, _idlePowerDrawMax, _turretMaxRange);
            }
        }

        void SetPowerSink()
        {
            sink = Entity.Components.Get<MyResourceSinkComponent>();

            MyResourceSinkInfo resourceInfo = new MyResourceSinkInfo()
            {
                ResourceTypeId = resourceId,
                MaxRequiredInput = IsTurret ? _idlePowerDrawMax : _idlePowerDrawBase,
                RequiredInputFunc = () => GetPowerInput()
            };

            sink.RemoveType(ref resourceInfo.ResourceTypeId);
            sink.Init(MyStringHash.GetOrCompute("Thrust"), resourceInfo);
            sink.AddType(ref resourceInfo);
        }

        float GetPowerInput(bool count = true)
        {
            var s = Settings.GetSettings(Entity);

            if (!block.Enabled && (!s.Recharging || !_isReloading))
                return 0f;

            if (!block.IsFunctional)
                return 0f;

            var requiredInput = IsTurret ? CalculateTurretPowerDraw(turret.Range) : _idlePowerDrawBase;
            if (!_isReloading || (MyAPIGateway.Session.SurvivalMode && _isReloading && !gun.GunBase.HasEnoughAmmunition()))
            {
                sink.SetMaxRequiredInputByType(resourceId, requiredInput);
                return requiredInput;
            }

            _ticksSinceLastReload += 1;

            if (!s.Recharging)
            {
                sink.SetMaxRequiredInputByType(resourceId, requiredInput);
                return requiredInput;
            }

            if (count) 
            {
                var suppliedRatio = sink.SuppliedRatioByType(resourceId);
                if (suppliedRatio == 1)
                    _currentReloadTicks += 1;
                else
                    _currentReloadTicks += 1 *  suppliedRatio * 0.5f; // nerfed recharge rate if overloaded
            }

            if (_currentReloadTicks >= _reloadTicks)
            {
                _isReloading = false;
                _currentReloadTicks = 0;
                _ticksSinceLastReload = 0;
                sink.SetMaxRequiredInputByType(resourceId, requiredInput);
                return requiredInput;
            }

            var scaledReloadPowerDraw = _reloadPowerDraw;
            requiredInput = Math.Max(requiredInput, scaledReloadPowerDraw);
            sink.SetMaxRequiredInputByType(resourceId, requiredInput);
            return requiredInput;
        }

        void ShowReloadMessage()
        {           
            var s = Settings.GetSettings(Entity);
            
            if (_isReloading && s.Recharging)
            {
                if (MyAPIGateway.Utilities.IsDedicated)
                    return;

                IMyShipController cockpit = MyAPIGateway.Session.Player?.Controller?.ControlledEntity?.Entity as IMyShipController;
                if (cockpit == null)
                {
                    IMyLargeTurretBase turret = MyAPIGateway.Session.Player?.Controller?.ControlledEntity?.Entity as IMyLargeTurretBase;
                    if (turret == null)
                    {
                        return;
                    }
                }

                if (_config.ShowReloadMessage)
                {
                    if (!(MyAPIGateway.Session.SurvivalMode && _isReloading && !gun.GunBase.HasEnoughAmmunition()))
                        MyAPIGateway.Utilities.ShowNotification($"{_config.ReloadMessage} ({100 * _currentReloadTicks / _reloadTicks:n0}%)", 16);
                }
            }
        }

        float _m = 0;
        float _b = 0;
        void GetTurretPowerDrawConstants(float start, float end, float maxRange)
        {
            _b = start;
            _m = (end - start) / (maxRange * maxRange * maxRange);
        }

        float CalculateTurretPowerDraw(float currentRange)
        {
            return _m * currentRange * currentRange * currentRange + _b;
        }
    }
}