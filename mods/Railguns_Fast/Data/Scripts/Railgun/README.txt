=============================
 Introduction
=============================

Howdy!

This document will describe how to use my weapon framework for your own weapons!

You will need a basic understanding of C# as well as a weapon model configured
with all the appropriate .sbc files required to make a regular  weapon mod work
in game.

Once condifigured, this framework will replace the vanilla firing and bullet logic
with my own :)

Note:
Fixed Guns must inherit from IMySmallMissileLauncher
Turrets muct inherit from IMyLargeGatlingTurret 

=============================
 Instructions
=============================

1.  Make a copy of FixedGunTemplate.cs or TurretTemplate.cs based on the type of
    weapon you are integrating with this framework

    1.  Change the file name to what you want

    2.  Change the class name to match the file name

    3.  Change the subtype name to match the subtype name your CubeBlocks.sbc defines

    4.  Change the MyObjectBuilder to the correct type based on weapon type:
        -   Fixed Gun: MyObjectBuilder_SmallMissileLauncher
        -   Turret   : MyObjectBuilder_LargeGatlingTurret 

2.  Open the file RailgunConstants.cs

    1.  Create a dictionary entry in RailgunConfigDict for your new weapon. Follow
        the example shown in the file. Delete any entries not being used by your
        mod (ie: Don't keep definitions in there that aren't in your mod).

        Also, DO NOT use the BASE railgun classes: RailgunFixedBase,
        RailgunTurretBase, or RailgunBase.

    2.  Configure all of the constants the way you want them.

    3.  Change both NETIDs to a unique number between 0 and 65535.

    4.  Change the DEBUG_MSG_TAG.

3. Replace the namespace Whiplash.Railgun with something unique in every file it appears

    *   You can use the python script change_namespace.py that is provided if you
        have python installed

    *   Otherwise you need to change the following files:
        -   Whip's Weapon Framework/ArmorPiercingProjectile.cs
        -   Whip's Weapon Framework/ArmorPiercingProjectileClient.cs
        -   Whip's Weapon Framework/RailgunBase.cs
        -   Whip's Weapon Framework/RailgunConfig.cs
        -   Whip's Weapon Framework/RailgunCore.cs
        -   Whip's Weapon Framework/RailgunTracerSync.cs
        -   Whip's Weapon Framework/ShieldApi.cs
        -   RailgunConstants.cs
        -   And any template class you are using

4. Remove any of the templates that you aren't using
 
5. That should be it! Load up the game and test it out :)
