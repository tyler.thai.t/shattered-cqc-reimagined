﻿using Sandbox.Common.ObjectBuilders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRage.Game.Components;


namespace Whiplash.Railgun
{
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_SmallMissileLauncher), false, "LargeRailGunLZM")]
    public class FixedRailgun : WeaponBlockFixedBase
    {
    }
}
