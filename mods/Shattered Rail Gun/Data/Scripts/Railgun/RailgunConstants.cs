﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whiplash.Railgun
{
    class RailgunConstants
    {
        public static RailgunConfig Config = new RailgunConfig()
        {
            ArtificialGravityMultiplier = 2f,
            NaturalGravityMultiplier = 1f,
            ShouldDrawProjectileTrails = true,
            PenetrationDamage = 33000,
            ExplosionRadius = 0f,
            ExplosionDamage = 0f,
            ShouldExplode = true,
            ShouldPenetrate = true,
            PenetrationRange = 100f,
            IdlePowerDrawFixed = 2f,
            IdlePowerDrawTurret = 20f,
            ReloadPowerDraw = 500f,
            MuzzleVelocity = 5000f,
            MaxRange = 2500,
            DeviationAngleDeg = 0.01f,
            ProjectileTracerScale = 5,
            RecoilImpulse = 1000000,
            ShieldDamageMultiplier = 5,
            RateOfFireRPM = 5,
        };

        // Tag for debugging
        public const string DEBUG_MSG_TAG = "Railgun Mod";

        // Config name
        public const string CONFIG_FILE_NAME = "RailgunConfig.sbc";

        // Network traffic IDs
        public const ushort NETID_TRACER = 1564;
        public const ushort NETID_RECHARGE_SYNC = 3896;

        // Subparts
        public const string SUBPART_FIXED_BARREL_NAME = "Barrel";
        public const string SUBPART_TURRET_BASE_NAME = "GatlingTurretBase1";
        public const string SUBPART_TURRET_ELEVATION_NAME = "GatlingTurretBase2";
        public const string SUBPART_TURRET_BARREL_NAME = "GatlingBarrel";

        // Bullet origin offsets
        public const double BULLET_SPAWN_FORWARD_OFFSET_FIXED = 0;
        public const double BULLET_SPAWN_FORWARD_OFFSET_TURRET = -3;

        // Subtypes
        public const string RAILGUN_FIXED_SUBTYPE_NAME = "LargeRailGunLZM";
        public const string RAILGUN_TURRET_SUBTYPE_NAME = "LargeRailgunTurretLZM";

        // Sound name
        public const string RAILGUN_SOUND_NAME = "WepShipLargeRailgunShotLZM";

        /* These keys should ALL be unique */
        public const string INI_SECTION_TAG = "Railgun Config";
        public const string INI_KEY_ART_GRAV = "Artificial gravity multiplier";
        public const string INI_KEY_NAT_GRAV = "Natural gravity multiplier";
        public const string INI_KEY_DRAW_TRAILS = "Draw projectile trails";
        public const string INI_KEY_PEN_DMG = "Penetration damage pool";
        public const string INI_KEY_EXP_RAD = "Explosion radius (m)";
        public const string INI_KEY_EXP_DMG = "Explosion damage";
        public const string INI_KEY_SHOULD_EXP = "Explode on contact";
        public const string INI_KEY_PEN = "Penetrate on contact";
        public const string INI_KEY_PEN_RANGE = "Penetration range";
        public const string INI_KEY_PWR_FIXED = "Fixed railgun idle power (MW)";
        public const string INI_KEY_PWR_TURRET = "Turret railgun idle power (MW)";
        public const string INI_KEY_PWR_RELOAD = "Railgun reload power (MW)";
        public const string INI_KEY_MUZZLE_VEL = "Muzzle velocity (m/s)";
        public const string INI_KEY_MAX_RANGE = "Max range (m)";
        public const string INI_KEY_DEVIANCE = "Spread angle (°)";
        public const string INI_KEY_TRACER_SCALE = "Tracer scale";
        public const string INI_KEY_RECOIL = "Recoil impulse";
        public const string INI_KEY_SHIELD_MULT = "Shield Damage Multiplier (for DarkStar's Shield Mod)";
        public const string INI_KEY_ROF = "Rate of fire (RPM)";
    }
}