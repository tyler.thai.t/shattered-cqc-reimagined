﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using VRage.Game;
using VRage.Game.Components;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using Sandbox.Game.Weapons;
using VRage.Game.ModAPI;
using VRageMath;
using Sandbox.Game;
using VRage.Game.Entity;
using Sandbox.Game.Entities;
using VRage.Game.ModAPI.Interfaces;
using Sandbox.Definitions;
using VRage;
using Whiplash.Railgun;
using VRage.Collections;
using VRage.Voxels;

namespace Whiplash.ArmorPiercingProjectiles
{
    public class ArmorPiercingProjectile
    {
        public RailgunTracerData CurrentTracer { get; private set; }
        public bool Killed { get; private set; } = false;
        public bool ShouldDrawTracer { get; private set; } = false;

        const float TrailDecayRatio = 0.97f;
        const float Tick = 1f / 60f;
        const int RaycastDelayTicks = 6;

        int _currentTracerDelayTicks = 0;
        Vector3 _tracerFrom;
        Vector3D _origin;
        Vector3D _lastPositionChecked;
        Vector3D _position;
        Vector3D _velocity;
        Vector3D _lastVelocity;
        Vector3D _direction;
        Vector3D _hitPosition;
        readonly float _explosionDamage;
        readonly float _shieldDamageMult;
        readonly float _explosionRadius;
        readonly float _projectileSpeed;
        readonly float _maxTrajectory;
        readonly float _minimumArmDistance = 0f;
        readonly float _penetrationRange;
        readonly float _penetrationDamage;
        readonly float _deviationAngle;
        int _checkIntersectionIndex = 5;
        bool _positionChecked = false;
        readonly bool _shouldExplode;
        readonly bool _shouldPenetrate;
        readonly bool _drawTrail;
        readonly bool _drawTracer;
        bool _targetHit = false;
        bool _penetratedObjectsSorted = false;
        bool _penetratedObjectsDamaged = false;
        readonly long _gunEntityID;
        Vector4 _trailColor;
        readonly MyStringId _material = MyStringId.GetOrCompute("WeaponLaser");
        readonly MyStringId _bulletMaterial = MyStringId.GetOrCompute("ProjectileTrailLine");
        readonly Vector3 _tracerColor;
        readonly float _tracerScale;
        readonly List<PenetratedEntityContainer> _objectsToPenetrate = new List<PenetratedEntityContainer>();
        readonly List<MyLineSegmentOverlapResult<MyEntity>> _overlappingEntities = new List<MyLineSegmentOverlapResult<MyEntity>>();
        readonly List<Vector3I> _hitPositions = new List<Vector3I>();
        readonly List<MyLineSegmentOverlapResult<MyVoxelBase>> _voxelOverlap = new List<MyLineSegmentOverlapResult<MyVoxelBase>>();
        readonly Queue<Vector3> _positionQueue = new Queue<Vector3>(RaycastDelayTicks + 1);
        readonly Queue<Vector3> _directionQueue = new Queue<Vector3>(RaycastDelayTicks);
        readonly Queue<bool> _shouldDrawQueue = new Queue<bool>(RaycastDelayTicks);
        Vector3D? _cachedSurfacePoint = null;
        readonly List<RailgunTracerData> _tracerData = new List<RailgunTracerData>();
        bool _hitMaxTrajectory = false;
        bool _finalTracerSet = false;

        public ArmorPiercingProjectile(RailgunFireData fireData)
        {
            // Weapon data
            _tracerColor = new Vector3(0, 0.5f, 1f); //TODO: ADD CONFIG SETTING fireData.ProjectileTrailColor;
            _trailColor = new Vector4(_tracerColor, 1f);
            _tracerScale = RailgunConstants.Config.ProjectileTracerScale;
            _maxTrajectory = RailgunConstants.Config.MaxRange;
            _projectileSpeed = RailgunConstants.Config.MuzzleVelocity;
            _deviationAngle = MathHelper.ToRadians(RailgunConstants.Config.DeviationAngleDeg);
            _gunEntityID = fireData.ShooterID;

            // Config data
            _drawTrail = RailgunConstants.Config.ShouldDrawProjectileTrails;
            _explosionDamage = RailgunConstants.Config.ExplosionDamage;
            _explosionRadius = RailgunConstants.Config.ExplosionRadius;
            _penetrationDamage = RailgunConstants.Config.PenetrationDamage;
            _penetrationRange = RailgunConstants.Config.PenetrationRange;
            _shouldExplode = RailgunConstants.Config.ShouldExplode;
            _shouldPenetrate = RailgunConstants.Config.ShouldPenetrate;
            _shieldDamageMult = RailgunConstants.Config.ShieldDamageMultiplier;

            // Fire data
            var temp = fireData.Direction;
            _direction = Vector3D.IsUnit(ref temp) ? temp : Vector3D.Normalize(temp);
            _direction = GetDeviatedVector(_direction, _deviationAngle);
            _origin = fireData.Origin;
            _position = _origin;
            _velocity = fireData.ShooterVelocity + _direction * _projectileSpeed;
            _lastVelocity = _velocity;
            _lastPositionChecked = _origin;

            _positionQueue.Enqueue(_origin);
        }

        public static Vector3 GetDeviatedVector(Vector3 direction, float deviationAngle)
        {
            float elevationAngle = MyUtils.GetRandomFloat(-deviationAngle, deviationAngle);
            float rotationAngle = MyUtils.GetRandomFloat(0f, MathHelper.TwoPi);
            Vector3 normal = -new Vector3(MyMath.FastSin(elevationAngle) * MyMath.FastCos(rotationAngle), MyMath.FastSin(elevationAngle) * MyMath.FastSin(rotationAngle), MyMath.FastCos(elevationAngle));
            var mat = Matrix.CreateFromDir(direction);
            return Vector3.TransformNormal(normal, mat);
        }

        public void Update()
        {
            if (_targetHit)
            {
                Kill();
                return;
            }

            // Update velocity due to gravity
            Vector3D totalGravity = MyParticlesManager.CalculateGravityInPoint(_position);
            Vector3D naturalGravity = RailgunCore.GetNaturalGravityAtPoint(_position);
            Vector3D artificialGravity = totalGravity - naturalGravity;
            _velocity += (naturalGravity * RailgunConstants.Config.NaturalGravityMultiplier + artificialGravity * RailgunConstants.Config.ArtificialGravityMultiplier) * Tick;

            // Update direction if velocity has changed
            if (!_velocity.Equals(_lastVelocity, 1e-3))
                _direction = Vector3D.Normalize(_velocity);

            _lastVelocity = _velocity;

            // Update position
            var oldPos = _position;
            _position += _velocity * Tick;
            var _toOrigin = _position - _origin;

            // Get important bullet parameters
            float lengthMultiplier = 40f * _tracerScale;
            lengthMultiplier *= 0.8f;

            bool shouldDraw = false;
            if (lengthMultiplier > 0f && !_targetHit && Vector3D.DistanceSquared(_position, _origin) > lengthMultiplier * lengthMultiplier && !Killed)
                shouldDraw = true;

            _positionQueue.Enqueue(_position);
            _directionQueue.Enqueue(_direction);
            _shouldDrawQueue.Enqueue(shouldDraw);

            if (_currentTracerDelayTicks < RaycastDelayTicks)
                ++_currentTracerDelayTicks;
            else
            {
                _tracerFrom = _positionQueue.Dequeue();
                Vector3 tracerTo = _positionQueue.Peek();
                Vector3 tracerDirection = _directionQueue.Dequeue();
                bool tracerDraw = _shouldDrawQueue.Dequeue();

                CurrentTracer = new RailgunTracerData()
                {
                    DrawTracer = tracerDraw,
                    DrawTrail = _drawTrail,
                    TracerColor = _tracerColor,
                    LineFrom = _tracerFrom,
                    LineTo = tracerTo,
                    ProjectileDirection = tracerDirection,
                    ProjectileTrailScale = _tracerScale,
                    TrailDecayRatio = TrailDecayRatio,
                };

                ShouldDrawTracer = true;
            }

            if (_hitMaxTrajectory)
            {
                _targetHit = true;
                _hitPosition = _position;
                Kill();
                if (_shouldExplode)
                    CreateExplosion(_position, _direction, _explosionRadius, _explosionDamage);
                return;
            }
            
            if (_toOrigin.LengthSquared() > _maxTrajectory * _maxTrajectory)
            {
                MyLog.Default.WriteLine(">> Max range hit");
                _hitMaxTrajectory = true;
                _positionChecked = false;
            }

            _checkIntersectionIndex = ++_checkIntersectionIndex % RaycastDelayTicks;
            if (_checkIntersectionIndex != 0 && _positionChecked)
            {
                return;
            }

            var to = _position;
            var from = _lastPositionChecked;
            _positionChecked = true;
            _lastPositionChecked = _position;

            IHitInfo hitInfo;
            bool hit = false;

            if (Vector3D.DistanceSquared(to, from) > 50 * 50)
            {
                // Use faster raycast if ray is long enough
                hit = MyAPIGateway.Physics.CastLongRay(from, to, out hitInfo, true);
            }
            else
            {
                hit = MyAPIGateway.Physics.CastRay(from, to, out hitInfo, 0);
            }

            // DS - Shield hit intersection and damage
            if (RailgunCore.Instance.ShieldApiLoaded)
            {
                var checkLine = new LineD(from, to);
                var shieldInfo = RailgunCore.Instance.ShieldApi.ClosestShieldInLine(checkLine, true);
                if (hit && shieldInfo.Item1.HasValue && hitInfo.Fraction * checkLine.Length > shieldInfo.Item1.Value || shieldInfo.Item1.HasValue)
                {
                    _hitPosition = from + (checkLine.Direction * shieldInfo.Item1.Value) + -0.5 * _direction;

                    float damage = _shieldDamageMult * (_explosionDamage + _penetrationDamage);
                    float currentCharge = RailgunCore.Instance.ShieldApi.GetCharge(shieldInfo.Item2);
                    float hpToCharge = RailgunCore.Instance.ShieldApi.HpToChargeRatio(shieldInfo.Item2);
                    float newCharge = currentCharge - (damage / hpToCharge);

                    // Deal damage
                    RailgunCore.Instance.ShieldApi.SetCharge(shieldInfo.Item2, newCharge);

                    // Draw impact
                    RailgunCore.Instance.ShieldApi.PointAttackShield(
                                            shieldInfo.Item2,
                                            _hitPosition,
                                            _gunEntityID,
                                            0f,
                                            false,
                                            true,
                                            false);

                    _targetHit = true;
                    Kill();
                    return;
                }
            }

            if (hit)
            {
                MyLog.Default.WriteLine(">> Raycast hit");

                _hitPosition = hitInfo.Position + -0.5 * _direction;

                if ((_hitPosition - _origin).LengthSquared() > _minimumArmDistance * _minimumArmDistance) //only explode if beyond arm distance
                {
                    if (_shouldExplode)
                        CreateExplosion(_hitPosition, _direction, _explosionRadius, _explosionDamage);

                    if (_shouldPenetrate)
                        GetObjectsToPenetrate(_hitPosition, _hitPosition + _direction * _penetrationRange);

                    _targetHit = true;
                    Kill();
                }
                else
                {
                    _targetHit = true;
                    _hitPosition = _position;
                    Kill();
                }
                return;
            }
            // implied else
            var line = new LineD(from, to);
            MyGamePruningStructure.GetVoxelMapsOverlappingRay(ref line, _voxelOverlap);
            foreach (var result in _voxelOverlap)
            {
                MyPlanet planet = result.Element as MyPlanet;
                MyVoxelMap voxelMap = result.Element as MyVoxelMap;
                //MyVoxelCoordSystems for iterating?
                if (planet != null)
                {
                    MyLog.Default.WriteLine(">>>>>\nPlanet voxel collision tests");
                    Vector3D startPos = from - planet.PositionLeftBottomCorner;
                    Vector3I startInt = Vector3I.Round(startPos);
                    Vector3D endPos = to - planet.PositionLeftBottomCorner;
                    Vector3I endInt = Vector3I.Round(endPos);

                    BresenhamLineDraw(startInt, endInt, voxelTestPoints);

                    MyLog.Default.WriteLine($"pts to scan: {voxelTestPoints.Count}");

                    for (int i = 0; i < voxelTestPoints.Count; ++i)
                    {
                        Vector3I voxelCoord = voxelTestPoints[i];
                        var voxelHit = new VoxelHit();
                        planet.Storage.ExecuteOperationFast(ref voxelHit, MyStorageDataTypeFlags.Content, ref voxelCoord, ref voxelCoord, notifyRangeChanged: false);
                        if (voxelHit.HasHit)
                        {
                            MyLog.Default.WriteLine($">> Railgun projectile hit planet at iteration {i}");
                            _hitPosition = (Vector3D)voxelCoord + planet.PositionLeftBottomCorner;

                            if (_shouldExplode)
                                CreateExplosion(_hitPosition, _direction, _explosionRadius, _explosionDamage);
                            _targetHit = true;
                            Kill();
                            return;
                        }
                    }
                    MyLog.Default.WriteLine("<<<<<");
                }
                else if (voxelMap != null)
                {
                    MyLog.Default.WriteLine(">>>>>\nAsteriod voxel collision tests");
                    Vector3D startPos = from - voxelMap.PositionLeftBottomCorner;
                    Vector3I startInt = Vector3I.Round(startPos);
                    Vector3D endPos = to - voxelMap.PositionLeftBottomCorner;
                    Vector3I endInt = Vector3I.Round(endPos);

                    BresenhamLineDraw(startInt, endInt, voxelTestPoints);

                    MyLog.Default.WriteLine($"pts to scan {voxelTestPoints.Count}");
                    for (int i = 0; i < voxelTestPoints.Count; ++i)
                    {
                        Vector3I voxelCoord = voxelTestPoints[i];
                        var voxelHit = new VoxelHit();
                        voxelMap.Storage.ExecuteOperationFast(ref voxelHit, MyStorageDataTypeFlags.Content, ref voxelCoord, ref voxelCoord, notifyRangeChanged: false);
                        if (voxelHit.HasHit)
                        {
                            MyLog.Default.WriteLine($">> Railgun projectile hit voxel at iteration {i}");
                            _hitPosition = (Vector3D)voxelCoord + voxelMap.PositionLeftBottomCorner;//_position;

                            if (_shouldExplode)
                                CreateExplosion(_hitPosition, _direction, _explosionRadius, _explosionDamage);
                            _targetHit = true;
                            Kill();
                            return;
                        }
                    }
                    MyLog.Default.WriteLine("<<<<<");
                }
            }

            
        }

        List<Vector3I> voxelTestPoints = new List<Vector3I>();

        void BresenhamLineDraw(Vector3I start, Vector3I end, List<Vector3I> points)
        {
            points.Clear();
            points.Add(start);
            Vector3I delta = end - start;
            Vector3I step = Vector3I.Sign(delta);
            delta *= step;
            int max = delta.AbsMax();

            if (max == delta.X)
            {
                int p1 = 2 * delta.Y - delta.X;
                int p2 = 2 * delta.Z - delta.X;
                while (start.X != end.X)
                {
                    start.X += step.X;
                    if (p1 >= 0)
                    {
                        start.Y += step.Y;
                        p1 -= 2 * delta.X;
                    }

                    if (p2 >= 0)
                    {
                        start.Z += step.Z;
                        p2 -= 2 * delta.X;
                    }
                    p1 += 2 * delta.Y;
                    p2 += 2 * delta.Z;
                    points.Add(start);
                }
            }
            else if (max == delta.Y)
            {
                int p1 = 2 * delta.X - delta.Y;
                int p2 = 2 * delta.Z - delta.Y;
                while (start.Y != end.Y)
                {
                    start.Y += step.Y;
                    if (p1 >= 0)
                    {
                        start.X += step.X;
                        p1 -= 2 * delta.Y;
                    }

                    if (p2 >= 0)
                    {
                        start.Z += step.Z;
                        p2 -= 2 * delta.Y;
                    }
                    p1 += 2 * delta.X;
                    p2 += 2 * delta.Z;
                    points.Add(start);
                }
            }
            else
            {
                int p1 = 2 * delta.X - delta.Z;
                int p2 = 2 * delta.Y - delta.Z;
                while (start.Z != end.Z)
                {
                    start.Z += step.Z;
                    if (p1 >= 0)
                    {
                        start.X += step.X;
                        p1 -= 2 * delta.Z;
                    }

                    if (p2 >= 0)
                    {
                        start.Y += step.Y;
                        p2 -= 2 * delta.Z;
                    }
                    p1 += 2 * delta.X;
                    p2 += 2 * delta.Y;
                    points.Add(start);
                }
            }
        }

        public struct VoxelHit : IVoxelOperator
        {
            public bool HasHit;

            public void Op(ref Vector3I pos, MyStorageDataTypeEnum dataType, ref byte content)
            {
                if (content != MyVoxelConstants.VOXEL_CONTENT_EMPTY)
                {
                    HasHit = true;
                }
            }

            public VoxelOperatorFlags Flags
            {
                get { return VoxelOperatorFlags.Read; }
            }
        }

        void CreateExplosion(Vector3D position, Vector3D direction, float radius, float damage, float scale = 1f)
        {
            var m_explosionFullSphere = new BoundingSphere(position, radius);

            MyExplosionInfo info = new MyExplosionInfo()
            {
                PlayerDamage = damage,
                Damage = damage,
                ExplosionType = MyExplosionTypeEnum.WARHEAD_EXPLOSION_02,
                ExplosionSphere = m_explosionFullSphere,
                LifespanMiliseconds = MyExplosionsConstants.EXPLOSION_LIFESPAN,
                ParticleScale = scale,
                Direction = direction,
                VoxelExplosionCenter = m_explosionFullSphere.Center,
                ExplosionFlags = MyExplosionFlags.AFFECT_VOXELS | MyExplosionFlags.CREATE_PARTICLE_EFFECT | MyExplosionFlags.APPLY_DEFORMATION,
                VoxelCutoutScale = 0.25f,
                PlaySound = true,
                ApplyForceAndDamage = true,
                ObjectsRemoveDelayInMiliseconds = 40
            };

            MyExplosions.AddExplosion(ref info);
        }

        void GetObjectsToPenetrate(Vector3D start, Vector3D end)
        {
            MyLog.Default.WriteLine($">>>>>>>>> Getting railgun penetrated objects START <<<<<<<<<");

            _objectsToPenetrate.Clear();
            var testRay = new LineD(start, end);
            MyGamePruningStructure.GetAllEntitiesInRay(ref testRay, _overlappingEntities);

            foreach (var hit in _overlappingEntities)
            {
                var destroyable = hit.Element as IMyDestroyableObject;
                if (destroyable != null)
                {
                    MyLog.Default.WriteLine($"Destroyable object found");
                    var penetratedEntity = new PenetratedEntityContainer()
                    {
                        PenetratedEntity = destroyable,
                        WorldPosition = hit.Element.PositionComp.GetPosition(),
                    };

                    _objectsToPenetrate.Add(penetratedEntity);
                    continue;
                }

                var grid = hit.Element as IMyCubeGrid;
                if (grid != null)
                {
                    MyLog.Default.WriteLine($"Cube grid found");
                    IMySlimBlock slimBlock;

                    grid.RayCastCells(start, end, _hitPositions);

                    if (_hitPositions.Count == 0)
                    {
                        MyLog.Default.WriteLine(" No slim block found in intersection");
                        continue;
                    }

                    MyLog.Default.WriteLine($" {_hitPositions.Count} slim blocks in intersection");

                    foreach (var position in _hitPositions)
                    {
                        slimBlock = grid.GetCubeBlock(position);
                        if (slimBlock == null)
                            continue;
 
                        var penetratedEntity = new PenetratedEntityContainer()
                        {
                            PenetratedEntity = slimBlock,
                            WorldPosition = Vector3D.Transform(position * grid.GridSize, grid.WorldMatrix),
                        };
                        _objectsToPenetrate.Add(penetratedEntity);
                    }
                    continue;
                }
            }

            MyLog.Default.WriteLine($"<<<<<<<<< Getting railgun penetrated objects END >>>>>>>>>");
        }

        void SortObjectsToPenetrate(Vector3D start)
        {
            MyLog.Default.WriteLine($">>>>>>>>> Sorting railgun penetrated objects START <<<<<<<<<");
            // Sort objects to penetrate by distance, closest first
            _objectsToPenetrate.Sort((x, y) => Vector3D.DistanceSquared(start, x.WorldPosition).CompareTo(Vector3D.DistanceSquared(start, y.WorldPosition)));
            MyLog.Default.WriteLine($"<<<<<<<<< Sorting railgun penetrated objects END >>>>>>>>>");
        }

        void DamageObjectsToPenetrate(float damage)
        {
            MyLog.Default.WriteLine(">>>>>>>>>> Railgun penetration START <<<<<<<<<<");
            MyLog.Default.WriteLine($"Railgun initial pooled damage: {damage}");

            foreach (var item in _objectsToPenetrate)
            {
                if (damage <= 0)
                {
                    MyLog.Default.WriteLine("> Pooled damage expended");
                    break;
                }

                var destroyableObject = item.PenetratedEntity;

                var slimBlock = destroyableObject as IMySlimBlock;
                if (slimBlock != null)
                {
                    MyLog.Default.WriteLine($"> Slim block found");

                    var blockIntegrity = slimBlock.Integrity;
                    var cube = slimBlock.FatBlock;
                    MyLog.Default.WriteLine($"cube type: {(cube == null ? "null" : cube.GetType().ToString())}");
                    MyLog.Default.WriteLine($"pooled damage before: {damage}");
                    MyLog.Default.WriteLine($"block integrity before: {blockIntegrity}");

                    var invDamageMultiplier = 1f;
                    var cubeDef = slimBlock.BlockDefinition as MyCubeBlockDefinition;
                    if (cubeDef != null)
                    {
                        MyLog.Default.WriteLine($"block damage mult: {cubeDef.GeneralDamageMultiplier}");
                        invDamageMultiplier = 1f / cubeDef.GeneralDamageMultiplier;
                    }

                    try
                    {
                        if (damage > blockIntegrity)
                            slimBlock.DoDamage(blockIntegrity * invDamageMultiplier, MyStringHash.GetOrCompute("Railgun"), false, default(MyHitInfo), _gunEntityID); //because some blocks have a stupid damage intake modifier
                        else
                            slimBlock.DoDamage(damage * invDamageMultiplier, MyStringHash.GetOrCompute("Railgun"), false, default(MyHitInfo), _gunEntityID);
                    }
                    catch (Exception ex)
                    {
                        MyLog.Default.WriteLine(ex);
                    }

                    if (damage < blockIntegrity)
                    {
                        damage = 0;
                        MyLog.Default.WriteLine($"pooled damage after: {damage}");
                        MyLog.Default.WriteLine($"block integrity after: {slimBlock.Integrity}");
                        break;
                    }
                    else
                    {
                        damage -= blockIntegrity;
                    }

                    MyLog.Default.WriteLine($"pooled damage after: {damage}");
                    MyLog.Default.WriteLine($"block integrity after: {slimBlock.Integrity}");

                    continue;
                }

                var character = destroyableObject as IMyCharacter;
                if (character != null)
                {
                    MyLog.Default.WriteLine($"> Character found");
                    character.Kill();
                    continue;
                }

                MyLog.Default.WriteLine($"> Destroyable entity found");
                var cachedIntegrity = destroyableObject.Integrity;

                destroyableObject.DoDamage(damage, MyStringHash.GetOrCompute("Railgun"), false, default(MyHitInfo), _gunEntityID);
                if (damage < cachedIntegrity)
                    damage = 0;
                else
                    damage -= cachedIntegrity;

            }
            MyLog.Default.WriteLine("<<<<<<<<<< Railgun penetration END >>>>>>>>>>");
        }

        void Kill()
        {
            if (!_finalTracerSet)
            {
                // Get important bullet parameters
                float lengthMultiplier = 40f * _tracerScale;
                lengthMultiplier *= 0.6f;

                bool shouldDraw = false;
                if (lengthMultiplier > 0f && !_targetHit && Vector3D.DistanceSquared(_position, _origin) > lengthMultiplier * lengthMultiplier && !Killed)
                    shouldDraw = true;

                CurrentTracer = new RailgunTracerData()
                {
                    DrawTracer = shouldDraw,
                    DrawTrail = _drawTrail,
                    TracerColor = _tracerColor,
                    LineFrom = ShouldDrawTracer ? _tracerFrom : (Vector3)_origin,
                    LineTo = _hitPosition,
                    ProjectileDirection = _direction,
                    ProjectileTrailScale = _tracerScale,
                    TrailDecayRatio = TrailDecayRatio,
                };
                ShouldDrawTracer = true;
                _finalTracerSet = true;
            }

            if (_shouldPenetrate && _targetHit)
            {
                if (!_penetratedObjectsSorted)
                {
                    SortObjectsToPenetrate(_hitPosition);
                    _penetratedObjectsSorted = true;
                    return;
                }

                if (!_penetratedObjectsDamaged)
                {
                    DamageObjectsToPenetrate(_penetrationDamage);
                    _penetratedObjectsDamaged = true;
                }
            }

            Killed = true;
        }

        public struct PenetratedEntityContainer
        {
            public IMyDestroyableObject PenetratedEntity;
            public Vector3D WorldPosition;
        }
    }
}
