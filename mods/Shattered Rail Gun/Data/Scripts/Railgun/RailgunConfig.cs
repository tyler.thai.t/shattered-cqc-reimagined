﻿namespace Whiplash.Railgun
{
    public struct RailgunConfig
    {
        public float ArtificialGravityMultiplier;
        public float NaturalGravityMultiplier;
        public bool ShouldDrawProjectileTrails;
        public bool ShouldExplode;
        public float ExplosionRadius;
        public float ExplosionDamage;
        public bool ShouldPenetrate;
        public float PenetrationDamage;
        public float PenetrationRange;
        public float IdlePowerDrawFixed;
        public float IdlePowerDrawTurret;
        public float ReloadPowerDraw;
        public float MuzzleVelocity;
        public float MaxRange;
        public float DeviationAngleDeg;
        public float RecoilImpulse;
        public float ProjectileTracerScale;
        public float ShieldDamageMultiplier;
        public float RateOfFireRPM;
    }
}

// fade rate
// Tracer color (vector3d)
// Offset for turret and fixed muzzle positions