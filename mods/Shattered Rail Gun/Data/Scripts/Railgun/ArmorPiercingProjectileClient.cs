﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using VRage.Game;
using VRage.Game.Components;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using VRage.Utils;
using Sandbox.Game.Weapons;
using VRage.Game.ModAPI;
using VRageMath;
using Sandbox.Game;
using VRage.Game.Entity;
using Sandbox.Game.Entities;
using VRage.Game.ModAPI.Interfaces;
using Sandbox.Definitions;
using Whiplash.Railgun;
using VRage.Collections;
using VRage.Voxels;

namespace Whiplash.ArmorPiercingProjectiles
{
    public class ArmorPiercingProjectileClient
    {
        public bool Remove { get; private set; } = false;
        const float TRAIL_KILL_THRESHOLD = 0.01f;
        readonly bool _drawTrail;
        readonly MyStringId _material = MyStringId.GetOrCompute("WeaponLaser");
        readonly MyStringId _bulletMaterial = MyStringId.GetOrCompute("ProjectileTrailLine");
        readonly Vector3 _tracerColor;
        readonly float _tracerScale;
        readonly Vector3D _from;
        readonly Vector3D _to;
        readonly Vector3D _direction;
        readonly float _trailDecayRatio;
        Vector4 _trailColor;
        readonly bool _drawFullTracer;
        bool _hasDrawnTracer = false;

        public ArmorPiercingProjectileClient(RailgunTracerData tracerData)
        {
            _tracerColor = tracerData.TracerColor;
            _trailColor = new Vector4(_tracerColor, 1f);
            _tracerScale = tracerData.ProjectileTrailScale;
            _from = tracerData.LineFrom;
            _to = tracerData.LineTo;
            _direction = tracerData.ProjectileDirection;
            _drawFullTracer = tracerData.DrawTracer;
            _drawTrail = tracerData.DrawTrail;
            _trailDecayRatio = tracerData.TrailDecayRatio;
        }

        public void DrawTracer()
        {
            if (MyAPIGateway.Utilities.IsDedicated)
            {
                Remove = true;
                return;
            }

            // Draw bullet trail
            if (_drawTrail)
            {
                MySimpleObjectDraw.DrawLine(_from, _to, _material, ref _trailColor, _tracerScale * 0.1f);
                _trailColor *= _trailDecayRatio;
                if (_trailColor.LengthSquared() < TRAIL_KILL_THRESHOLD)
                    Remove = true;        
            }
            else if (_hasDrawnTracer)
            {
                Remove = true;
            }

            // Draw tracer
            if (!_hasDrawnTracer)
            {
                float lengthMultiplier = 0;
                Vector3D startPoint = Vector3D.Zero;

                if (_drawFullTracer)
                {
                    lengthMultiplier = 0.6f * 40f * _tracerScale;
                    startPoint = _to - _direction * lengthMultiplier;
                }
                else
                {
                    startPoint = _from;
                    lengthMultiplier = (float)Vector3D.Distance(_to, _from);
                }

                float scaleFactor = MyParticlesManager.Paused ? 1f : MyUtils.GetRandomFloat(1f, 2f);
                float thickness = (MyParticlesManager.Paused ? 0.2f : MyUtils.GetRandomFloat(0.2f, 0.3f)) * _tracerScale;
                thickness *= MathHelper.Lerp(0.2f, 0.8f, 1f);

                MyTransparentGeometry.AddLineBillboard(_bulletMaterial, new Vector4(_tracerColor * scaleFactor * 10f, 1f), startPoint, _direction, lengthMultiplier, thickness);
                _hasDrawnTracer = true;
            }          
        }
    }
}
