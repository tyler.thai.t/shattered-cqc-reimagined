using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sandbox.Common;
using Sandbox.Common.ObjectBuilders;
using Sandbox.Common.ObjectBuilders.Definitions;
using Sandbox.Definitions;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.EntityComponents;
using Sandbox.Game.GameSystems;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using VRage.ObjectBuilders;
using Sandbox.Game.Gui;
using VRage.Utils;
using VRageMath;

namespace MWI2_Targeting
{
    // add your subtypes >
    [MyEntityComponentDescriptor(typeof(MyObjectBuilder_LargeGatlingTurret), false, "EX1PointDefense", "EXODPointDefense")]
    public class EX1PointDefense_Target : MyGameLogicComponent
    {

        private IMyCubeBlock block;

        protected string TargetMeteors = "TargetMeteors_On";
        protected string TargetMissiles = "TargetMissiles_On";
        protected string TargetSmallShips = "TargetSmallShips_Off";
        protected string TargetLargeShips = "TargetLargeShips_Off";
        protected string TargetCharacters = "TargetCharacters_Off";
        protected string TargetStations = "TargetStations_Off";
        protected string TargetNeutrals = "TargetNeutrals_Off";
        public int counter;
        //private int countDown = 60;
        //private int countCurrent = 0;
        private bool doSetup = true;
        private bool initSetup = true;

        public override void Init(MyObjectBuilder_EntityBase objectBuilder)
        {
            NeedsUpdate = MyEntityUpdateEnum.BEFORE_NEXT_FRAME;
            block = (IMyCubeBlock)Entity;
            //MyAPIGateway.Utilities.ShowNotification("[Init Done]", 1000, MyFontEnum.Blue);
        }


        public override void UpdateOnceBeforeFrame()
        {


            if (block.CubeGrid?.Physics == null) // ignore projected and other non-physical grids
                return;


            counter = 0;
            NeedsUpdate = MyEntityUpdateEnum.EACH_FRAME | MyEntityUpdateEnum.EACH_100TH_FRAME; // allow UpdateAfterSimulation() and UpdateAfterSimulation100() to execute, remove if not needed
        }


        public override void UpdateAfterSimulation100()
        {

            counter++;//ensure that its missiles only
            if (counter>=60) { doSetup = true; }

            if (doSetup)
            {
                //MyAPIGateway.Utilities.ShowNotification("[Trying Setup]", 1000, MyFontEnum.Blue);
                try
                {
                    //block = Entity as IMyCubeBlock;


                    if (doSetup)
                    {
                        // ignore projected grids
                        var placed = block as IMyLargeTurretBase;

                        if (placed != null)
                        {

                            //MyAPIGateway.Utilities.ShowNotification("[ ExtraTurret Valid ]", 4000, MyFontEnum.Blue);
                            placed.ApplyAction(TargetMeteors);
                            placed.ApplyAction(TargetMissiles);
                            placed.ApplyAction(TargetSmallShips);
                            placed.ApplyAction(TargetLargeShips);
                            
                            placed.ApplyAction(TargetStations);
                            placed.ApplyAction(TargetNeutrals);

                            if (initSetup) { placed.ApplyAction(TargetCharacters); initSetup = false; }

                            

                        
                        }

                        doSetup = false;
                        counter = 0;
                    }

                }
                catch (Exception exc)
                {

                    MyAPIGateway.Utilities.ShowNotification("" + exc.Message + "", 1000, MyFontEnum.Blue);

                }


            }
        }
    }
}